::::::::::::::::::::::::::::::::::::::::::::::::::
~~~~~~~~~~~~~~~~ POSTGRES SCRIPT ~~~~~~~~~~~~~~~~~
::::::::::::::::::::::::::::::::::::::::::::::::::


: << 'COMMENT'

-- Database: practicespring

-- DROP DATABASE IF EXISTS practicespring;

CREATE DATABASE practicespring
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;

COMMENT

::::::::::::::::::::::::::::::::::::::::::::::::::
~~~~~~~~~~~~~~~~ TEST WITH POSTMAN ~~~~~~~~~~~~~~~
::::::::::::::::::::::::::::::::::::::::::::::::::

... ${POST} < http://localhost:8080/api/v1/postRole >
{
    "roleName": "administrator"
}

{
    "roleName": "counter"
}
... ${POST} < http://localhost:8080/api/v1/postState >

{
    "stateName": "active"
}

{
    "stateName": "inactive"
}

...  ${POST} < http://localhost:8080/api/v1/postUser >

{
    "userName": "r0b0t95",
    "password": "1234qwerQWER!",
    "goals": 3,
    "birthDate": "2010-10-11",
    "role": {
        "id": 1
    },
    "state": {
        "id": 1
    }
}

... ${GET} < http://localhost:8080/api/v1/getUsers >


... ${PATCH} < http://localhost:8080/api/v1/patchUser/1 >

{
    "userName": "r0b0t95",
    "password": "1234",
    "goals": 3,
    "birthDate": "2010-10-11",
    "role": {
        "id": 1
    },
    "state": {
        "id": 2
    }
}
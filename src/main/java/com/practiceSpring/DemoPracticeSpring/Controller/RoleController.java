package com.practiceSpring.DemoPracticeSpring.Controller;

import com.practiceSpring.DemoPracticeSpring.Models.Role;
import com.practiceSpring.DemoPracticeSpring.Repositories.RoleRepository;
import com.practiceSpring.DemoPracticeSpring.Services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Transactional
@RequestMapping(path = "/api/v1")
public class RoleController implements RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleController(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @GetMapping(path = "/getRoles")
    @Override
    public List<Role> findRoles() {
        return roleRepository.findAll();
    }

    @GetMapping(path = "/getRole/{id}")
    @Override
    public Role findRoleById(Long id) {
        return roleRepository.findById(id).get();
    }

    @PostMapping(path = "/postRole")
    @Override
    public Role saveRole(Role role) {
        return roleRepository.save(role);
    }

    @PatchMapping(path = "/patchRole/{id}")
    @Override
    public Role updateRole(Long id, Role role) {
        Role updated = roleRepository.findById(id).get();
        updated.setRoleName(updated.getRoleName());
        return roleRepository.save(updated);
    }

    //2 NEW METHODS

    @GetMapping(path = "/getRoleName")
    @Override
    public List<Role> findAllRoleNames() {
        return roleRepository.findRoleNames();
    }

    @GetMapping(path = "/getByName/{roleName}")
    @Override
    public Role findRoleByName(String roleName) {
        return roleRepository.findRoleByName(roleName);
    }


}

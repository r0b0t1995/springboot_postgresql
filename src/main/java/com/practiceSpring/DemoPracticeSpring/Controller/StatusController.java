package com.practiceSpring.DemoPracticeSpring.Controller;

import com.practiceSpring.DemoPracticeSpring.Models.State;
import com.practiceSpring.DemoPracticeSpring.Repositories.StateRepository;
import com.practiceSpring.DemoPracticeSpring.Services.StateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Transactional
@RequestMapping(path = "/api/v1")
public class StatusController implements StateService {

    private final StateRepository stateRepository;

    @Autowired
    public StatusController(StateRepository stateRepository) {
        this.stateRepository = stateRepository;
    }

    @GetMapping(path = "/getStatus")
    @Override
    public List<State> findStatus() {
        return stateRepository.findAll();
    }

    @GetMapping(path = "/getState/{id}")
    @Override
    public State findStateById(Long id) {
        return stateRepository.findById(id).get();
    }

    @PostMapping(path = "/postState")
    @Override
    public State saveState(State state) {
        return stateRepository.save(state);
    }

    @PatchMapping(path = "/patchState/{id}")
    @Override
    public State updateState(Long id, State state) {
        State updated = stateRepository.findById(id).get();
        updated.setStateName(state.getStateName());
        return stateRepository.save(updated);
    }


}

package com.practiceSpring.DemoPracticeSpring.Controller;

import com.practiceSpring.DemoPracticeSpring.Encrypt;
import com.practiceSpring.DemoPracticeSpring.Models.User;
import com.practiceSpring.DemoPracticeSpring.Repositories.UserRepository;
import com.practiceSpring.DemoPracticeSpring.Services.UserService;
import com.practiceSpring.DemoPracticeSpring.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Transactional
@RequestMapping(path = "/api/v1")
public class UserController implements UserService {

    private final UserRepository userRepository;
    private final Encrypt encrypt = new Encrypt();
    private final Validate validate = new Validate();

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping(path = "/getUsers")
    @Override
    public List<User> findUser() {
        return userRepository.findAll();
    }

    @GetMapping(path = "/getUser/{id}")
    @Override
    public User findUserById(Long id) {
        return userRepository.findById(id).get();
    }

    @PostMapping(path = "/postUser")
    @Override
    public User saveUser(User user) {
        User userObject = null;
        if(validate.validatePassword(user.getPassword())) {
            user.setPassword(encrypt.encryptMethod(user.getPassword()));
            userObject = userRepository.save(user);
        }
        return userObject;
    }

    @PatchMapping(path = "/patchUser/{id}")
    @Override
    public User updateUser(Long id, User user) {
        User userObject = null;
        User updated = userRepository.findById(id).get();
        if(validate.validatePassword(user.getPassword())){
            updated.setUserName(user.getUserName());
            updated.setPassword(encrypt.encryptMethod(updated.getPassword()));
            updated.setGoals(user.getGoals());
            updated.setBirthDate(user.getBirthDate());
            updated.setRole(user.getRole());
            updated.setState(updated.getState());
            userObject = userRepository.save(updated);
        }
        return userObject;
    }

    // THIS METHOD CHANGE THE STATE = active to inactive :)
    @PatchMapping(path = "/deleteUser/{id}")
    @Override
    public User deleteUser(Long id, User user) {
        User updated = userRepository.findById(id).get();
        updated.setState(user.getState());
        return userRepository.save(updated);
    }
}

package com.practiceSpring.DemoPracticeSpring.Models;

import jakarta.persistence.*;
import lombok.NoArgsConstructor;

@Entity(name = "roles")
@Table(name = "roles")
@NoArgsConstructor
public class Role {

    @Id
    @Column(name = "roleId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "roleName", unique = true, nullable = false)
    private String roleName;

    //METHODS SET AND GET

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}

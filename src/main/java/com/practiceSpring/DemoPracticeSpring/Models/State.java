package com.practiceSpring.DemoPracticeSpring.Models;

import jakarta.persistence.*;
import lombok.NoArgsConstructor;

@Entity(name = "status")
@Table(name = "status")
@NoArgsConstructor
public class State {

    @Id
    @Column(name = "stateId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "stateName", unique = true, nullable = false)
    private String stateName;

    //METHODS SET AND GET

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
}

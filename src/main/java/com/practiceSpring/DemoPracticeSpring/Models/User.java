package com.practiceSpring.DemoPracticeSpring.Models;

import jakarta.persistence.*;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Entity(name = "users")
@Table(name = "users")
@NoArgsConstructor
public class User {

    @Id
    @Column(name = "userID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "userName", unique = true, nullable = false)
    private String userName;

    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "goals")
    private int goals;

    @Column(name = "birthDate")
    private Date birthDate;

    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "roleFk", referencedColumnName = "roleId")
    private Role role;

    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "stateFk", referencedColumnName = "stateId")
    private State state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getGoals() {
        return goals;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}

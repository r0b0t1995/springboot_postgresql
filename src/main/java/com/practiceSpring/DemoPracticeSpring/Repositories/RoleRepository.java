package com.practiceSpring.DemoPracticeSpring.Repositories;

import com.practiceSpring.DemoPracticeSpring.Models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("roleRepository")
public interface RoleRepository extends JpaRepository<Role, Long> {

    @Modifying
    @Transactional
    @Query("SELECT r FROM roles r")
    List<Role> findRoleNames();

    @Modifying
    @Transactional
    @Query("SELECT r FROM roles r WHERE r.roleName = ?1")
    Role findRoleByName(String roleName);

}

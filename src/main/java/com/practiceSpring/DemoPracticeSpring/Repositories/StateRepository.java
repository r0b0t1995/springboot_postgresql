package com.practiceSpring.DemoPracticeSpring.Repositories;

import com.practiceSpring.DemoPracticeSpring.Models.State;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("stateRepository")
public interface StateRepository extends JpaRepository<State, Long> {
}

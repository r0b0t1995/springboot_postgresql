package com.practiceSpring.DemoPracticeSpring.Services;

import com.practiceSpring.DemoPracticeSpring.Models.Role;
import com.practiceSpring.DemoPracticeSpring.Repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service("roleService")
public interface RoleService {

    List<Role> findRoles();

    Role findRoleById(@PathVariable Long id);

    Role saveRole(@RequestBody Role role);

    Role updateRole(@PathVariable Long id, @RequestBody Role role);

    //NEW 2 METHODS

    List<Role> findAllRoleNames();

    Role findRoleByName(@PathVariable String roleName);
}

package com.practiceSpring.DemoPracticeSpring.Services;

import com.practiceSpring.DemoPracticeSpring.Models.State;
import com.practiceSpring.DemoPracticeSpring.Repositories.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service("stateService")
public interface StateService {

    List<State> findStatus();

    State findStateById(@PathVariable Long id);

    State saveState(@RequestBody State state);

    State updateState(@PathVariable Long id, @RequestBody State state);
}

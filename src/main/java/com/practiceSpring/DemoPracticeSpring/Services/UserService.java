package com.practiceSpring.DemoPracticeSpring.Services;

import com.practiceSpring.DemoPracticeSpring.Models.User;
import com.practiceSpring.DemoPracticeSpring.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service("userService")
public interface UserService {

    List<User> findUser();

    User findUserById(@PathVariable Long id);

    User saveUser(@RequestBody User user);

    User updateUser(@PathVariable Long id, @RequestBody User user);

    User deleteUser(@PathVariable Long id, @RequestBody User user);
}

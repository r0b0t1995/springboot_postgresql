package com.practiceSpring.DemoPracticeSpring;

import java.util.regex.Pattern;
public class Validate {

    private final String passwordRegex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*_,.=+:/~]).{8,50}$";

    public boolean validatePassword(String password){
        boolean r = false;
        if(Pattern.matches(passwordRegex, password)){
            r = true;
        }
        return r;
    }


}
